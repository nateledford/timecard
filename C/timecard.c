#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct time_card_block
{
    struct tm* block_date;
    struct tm* clock_in;
    struct tm* clock_out;
    struct time_card_block* next;
} time_card_block;

time_card_block* create_time_card_block(time_card_block* next);
void view_time_card(time_card_block* block);
void print_time_card_block(time_card_block* block);
void print_date(struct tm* date);
void print_time(struct tm* time);

int main(int argc, char* argv[])
{
    printf("Timecard Version 0.0.1\n");

    time_card_block* root;
    time_card_block* current;

    printf("Time card not found. Please create first time card block.\n\n");
    root = create_time_card_block(NULL);
    current = root;

    int option = -1;
    do
    {
        printf("What would you like to do?\n");
        printf("[1] View Time Card\n");
        printf("[2] Prepend block to time card\n");
        printf("[0] Exit Application\n");
        scanf("%d", &option);

        switch (option)
        {
        case 0:
            break;
        case 1:
            view_time_card(current);
            break;
        case 2:
            current = create_time_card_block(current);
        default:
            printf("The option you entered is not valid. Please try again.");
            break;
        }

        printf("\n\n");
    }
    while (option != 0);

    return 0;
}

time_card_block* create_time_card_block(time_card_block* next)
{
    time_card_block* new_block;
    new_block = (time_card_block*)malloc(sizeof(time_card_block));

    if (new_block == NULL)
    {
        printf("Failed to create new time card block.");
        return NULL;
    }

    new_block->block_date = malloc(sizeof(struct tm));
    new_block->clock_in = malloc(sizeof(struct tm));
    new_block->clock_out = malloc(sizeof(struct tm));
    new_block->next = next;

    printf("Enter the date (mm dd): ");
    scanf("%d %d",
          &new_block->block_date->tm_mon,
          &new_block->block_date->tm_mday);

    printf("Enter the clock-in time (hh mm): ");
    scanf("%d %d",
          &new_block->clock_in->tm_hour,
          &new_block->clock_in->tm_min);

    printf("Enter the clock-out time (hh mm): ");
    scanf("%d %d",
          &new_block->clock_out->tm_hour,
          &new_block->clock_out->tm_min);

    return new_block;
}

void view_time_card(time_card_block* block)
{
    print_time_card_block(block);
    if (block->next == NULL)
    {
        return;
    }
    view_time_card(block->next);
}

void print_time_card_block(time_card_block* block)
{
    printf("Block Date:\t");
    print_date(block->block_date);

    printf("Clocked In:\t");
    print_time(block->clock_in);

    printf("Clocked Out:\t");
    print_time(block->clock_out);

    printf("\n");
}

void print_date(struct tm* date)
{
    char buffer[26];
    strftime(buffer, 26, "%m/%d", date);
    puts(buffer);
}

void print_time(struct tm* time)
{
    char buffer[26];
    strftime(buffer, 26, "%H:%M", time);
    puts(buffer);
}

