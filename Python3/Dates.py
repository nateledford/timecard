from datetime import datetime, timedelta


class Dates:
    """Provides custom methods for handling dates, times, and timedeltas in Python"""
    @staticmethod
    def build_date(d):
        """Builds a date from a user-provided string

        :returns:   A datetime object with the current year and user-supplied month and day
        """
        # Split the provided date (e.g., "05/15")
        split = d.split('/')
        month = int(split[0])
        day = int(split[1])

        result = datetime(datetime.now().year, month, day)
        return result

    @staticmethod
    def build_time(t, month, day):
        """Builds datetime object from a user-provided string

        :param t:       The time as entered by the user
        :param month:   The current month from the constructed date
        :param day:     The current day from the constructed date
        """
        split = t.split(':')
        hours = int(split[0])
        mins = int(split[1])

        result = datetime(datetime.now().year, month, day, hours, mins)

        return result

    @staticmethod
    def convert_timedelta(td):
        """Converts timedelta into a string

        :param td:  The timedelta to convert
        """
        days = td.days
        hours = td.seconds // 3600
        minutes = td.seconds // 60 % 60

        new_hours = str(days * 24 + hours)

        if minutes < 10:
            new_minutes = '0' + str(minutes)
        else:
            new_minutes = str(minutes)

        result = new_hours + ':' + new_minutes

        return result

    # @staticmethod
    # def round_time(time, base=3):
    #     return int(base * round(float(time)/base))

    @staticmethod
    def round_time(dt=None, date_delta=timedelta(minutes=6), to='average'):
        """
        Round a datetime object to a multiple of a timedelta
        dt : datetime.datetime object, default now.
        dateDelta : timedelta object, we round to a multiple of this, default 1 minute.
        from:  http://stackoverflow.com/questions/3463930/how-to-round-the-minute-of-a-datetime-object-python
        """
        round_to = date_delta.total_seconds()

        if dt is None:
            dt = datetime.now()
        seconds = (dt - dt.min).seconds

        if to == 'up':
            # // is a floor division, not a comment on following line (like in javascript):
            rounding = (seconds + round_to) // round_to * round_to
        elif to == 'down':
            rounding = seconds // round_to * round_to
        else:
            rounding = (seconds + round_to / 2) // round_to * round_to

        return dt + timedelta(0, rounding - seconds, -dt.microsecond)
