from datetime import datetime


class Json:
    """Provides custom methods for handling JSON"""
    @staticmethod
    def serialize_date(obj):
        """JSON serializer for objects not serializable by default json code
        :param obj:     The object to serialize
        """
        if isinstance(obj, datetime):
            serial = obj.isoformat()
            return serial
        raise TypeError("Type not serializable")
