import json
import os
from datetime import timedelta, datetime

import dateutil.parser as parser

from Dates import Dates
from Json import Json


class TimeCard:
    """Provides methods and variables for editing a time card"""
    timecard_blocks = []
    min_hours = 40
    f_name = 'timecard.json'

    @staticmethod
    def load_time_card():
        """Loads a time card stored in a JSON file from the hard disk if it can be found"""
        if os.path.isfile(TimeCard.f_name):
            # Time card file was found, so open the document
            with open(TimeCard.f_name) as infile:
                blocks = json.load(infile)
                for b in blocks:
                    t = TimeCardBlock.load(b)
                    t.load(b)
                    TimeCard.timecard_blocks.append(t)
        else:
            print('Time card file does not exist.')

    @staticmethod
    def save_time_card():
        """Saves an indented JSON file containing the time card to the hard disk"""
        with open(TimeCard.f_name, 'w') as outfile:
            to_dump = [b.dump() for b in TimeCard.timecard_blocks]
            json.dump(to_dump, outfile, indent=2)

    @staticmethod
    def delete_time_card():
        verify = input("Are you sure you wish to delete your current time card? (y/n): ")

        if verify == 'y' or verify == 'Y':
            os.remove(TimeCard.f_name)
            print('Time card was deleted')

    @staticmethod
    def view_time_card():
        """Prints the time card by block to the console"""
        for b in TimeCard.timecard_blocks:
            print(b.get_block())
        TimeCard.calculate_hours_worked()

    @staticmethod
    def calculate_hours_worked():
        """Calculates the number of hours (and minutes) worked so far"""
        time_worked = None
        for b in TimeCard.timecard_blocks:
            if time_worked is None:
                time_worked = b.get_delta()
            else:
                time_worked += b.get_delta()

        # Use method to convert time_worked to desired output format
        hours_worked = Dates.convert_timedelta(time_worked)
        print('\nHours worked (Rounded to nearest six minutes):')
        print(hours_worked)

        remaining = timedelta(hours=TimeCard.min_hours) - time_worked
        hours_left = Dates.convert_timedelta(remaining)
        hl_max = Dates.convert_timedelta((remaining + timedelta(minutes=3)))
        print('Hours Remaining (Rounded to nearest six minutes):')
        print(hours_left, 'to', hl_max)

        TimeCard.calculate_clockout_time(remaining)

    @staticmethod
    def calculate_clockout_time(hours_left):
        """If the current date is Friday, returns the time the user should clock out to avoid exceeding forty hours in
            a single workweek

            :param hours_left:  The number of hours remaining in the workweek
        """
        current_day = datetime.today().isoweekday()
        if current_day is 5:
            friday_blocks = [b for b in TimeCard.timecard_blocks if b.date.date() == datetime.today().date()]

            if len(friday_blocks) > 0:
                last_clock_out = friday_blocks[0].end_time
                new_clock_out_time = last_clock_out + timedelta(minutes=30) + hours_left
                print('You should clock out by', new_clock_out_time.strftime("%H:%M"), "today.")


class TimeCardBlock:
    """Represents one block (a date, a clock-in time, and a clock-out time) in a time card"""

    def __init__(self, date, start_time, end_time):
        """
        :param date:        The date the clock-in/clock-out occurred
        :param start_time:  Time when user clocked in
        :param end_time:    Time when user clocked out
        """
        self.date = date
        self.start_time = start_time
        self.end_time = end_time

    def get_delta(self):
        """Calculates the timedelta of the time card block"""
        end_rounded = Dates.round_time(self.end_time)
        start_rounded = Dates.round_time(self.start_time)
        return end_rounded - start_rounded

    def get_block(self):
        """Display the current time card block via a formatted string"""
        b_date = self.date.strftime('%m/%d/%Y')
        b_start = self.start_time.strftime('%H:%M')
        b_end = self.end_time.strftime('%H:%M')

        b_str = 'Date: ' + b_date + '\tIn: ' + b_start + '\tOut: ' + b_end
        return b_str

    def dump(self):
        """Formats the time card block to be stored in a JSON document"""
        return {
            'block': {
                'date': Json.serialize_date(self.date),
                'in_time': Json.serialize_date(self.start_time),
                'out_time': Json.serialize_date(self.end_time)
            }
        }

    @staticmethod
    def load(dumped_obj):
        """Deserializes data stored in a JSON document
        :param dumped_obj: The JSON data to be deserialized
        """
        date = parser.parse(dumped_obj['block']['date'])
        in_time = parser.parse(dumped_obj['block']['in_time'])
        out_time = parser.parse(dumped_obj['block']['out_time'])

        return TimeCardBlock(date,
                             in_time,
                             out_time)

    @staticmethod
    def build_timecard_block():
        """Builds a time card block via user-supplied data"""
        in_date = input("Enter the date (mm/dd): ")
        out_date = Dates.build_date(in_date)

        clock_in = input("What time did you clock in? ")
        in_t = Dates.build_time(clock_in, out_date.today().month, out_date.today().day)

        clock_out = input("What time did you clock out? ")
        out_t = Dates.build_time(clock_out, out_date.today().month, out_date.today().day)

        return TimeCardBlock(out_date, in_t, out_t)

    @staticmethod
    def edit_timecard_block():
        """Allows a time card block to be edited.  The time card block is essentially overwritten"""
        print('Which time card block do you wish to edit?')

        index = 1
        for b in TimeCard.timecard_blocks:
            print(index, ' ', b.get_block())
            index += 1

        selected_index = int(input())

        # Create new time card block
        new_block = TimeCardBlock.build_timecard_block()

        # Replace the selected time card block with the new time card block
        TimeCard.timecard_blocks[selected_index - 1] = new_block
        TimeCard.save_time_card()

        print('Block updated successfully!')

    @staticmethod
    def delete_timecard_block():
        print('Which time card block do you wish to delete?')

        index = 1
        for b in TimeCard.timecard_blocks:
            print(index, ' ', b.get_block())
            index += 1

        selected_index = int(input())

        block_to_remove = TimeCard.timecard_blocks[selected_index - 1]
        TimeCard.timecard_blocks.remove(block_to_remove)
        TimeCard.save_time_card()
