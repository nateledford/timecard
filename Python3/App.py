from Timecard import TimeCard, TimeCardBlock

version = '1.1.0'


class App:
    def __init__(self):
        # If true, the program will quit
        self.stop = False

        # Loads "timecard.json" from the hard disk if it exists
        TimeCard.load_time_card()

        # Display application name and version on first load only
        print('Timecard Calculator ', version)

    def run(self):
        while self.stop is False:
            print('\nWhat would you like to do?')
            print('[1] Add block to timecard')
            print('[2] Edit block')
            print('[3] Delete block')
            print('[4] View Time Card')
            print('[5] View Number of Hours Worked')
            print('[6] Delete Time Card')
            print('[0] Quit application')
            opt = input()

            # Exit program
            if opt is '0':
                self.stop = True
                return

            # Add block to time card
            elif opt is '1':
                TimeCard.timecard_blocks.append(TimeCardBlock.build_timecard_block())
                TimeCard.timecard_blocks.sort(key=lambda timecardblock: timecardblock.date)
                TimeCard.save_time_card()

            # Edit an existing block in the time card
            elif opt is '2':
                TimeCardBlock.edit_timecard_block()

            # Delete block
            elif opt is '3':
                TimeCardBlock.delete_timecard_block()

            # View the current time card, including number of hours worked
            elif opt is '4':
                print('Current Time Card:')
                TimeCard.view_time_card()

            # Only view the number of hours worked
            elif opt is '5':
                TimeCard.calculate_hours_worked()

            # Delete time card
            elif opt is '6':
                TimeCard.delete_time_card()

            # An invalid option was provided
            else:
                print('Selected option is not valid. Please try again.')


if __name__ == '__main__':
    app = App()
    app.run()
